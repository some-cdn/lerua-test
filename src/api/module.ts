import {Module} from "@nestjs/common";
import {JobController} from "./controller/job.controller";
import {JobService} from "./service/job.service";

@Module({
    imports: [],
    providers: [JobService],
    controllers: [JobController]
})
export class ApiModule {

}
