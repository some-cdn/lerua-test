import {Body, Controller, Delete, Get, HttpCode, Param, Post, Put} from "@nestjs/common";
import {JobService} from "../service/job.service";
import {CreateJobDto, UpdateJobDto} from "../dto/job.dto";
import {Job} from "../../schedule/job/job";
import {ApiResponse} from "@nestjs/swagger";

@Controller('jobs')
export class JobController {
    constructor(
        private jobService: JobService,
    ) {
    }

    @Get()
    @ApiResponse({
        type: Job,
        isArray: true,
        status: 200
    })
    @HttpCode(200)
    getAll(): Promise<Job[]> {
        return this.jobService.getAll();
    }

    @Get('/:id')
    @ApiResponse({
        type: Job,
        status: 200
    })
    @HttpCode(200)
    getOne(@Param('id') id: string): Promise<Job> {
        return this.jobService.getOne(id);
    }

    @Put('/:id')
    @ApiResponse({
        type: Job,
        status: 200
    })
    @HttpCode(200)
    updateOne(@Param('id') id: string, @Body() dto: UpdateJobDto): Promise<Job> {
        return this.jobService.updateOne(id, dto);
    }

    @Post()
    @ApiResponse({
        type: Job,
        status: 201
    })
    @HttpCode(201)
    createOne(@Body() dto: CreateJobDto): Promise<Job> {
        return this.jobService.createOne(dto);
    }

    @Delete('/:id')
    @ApiResponse({
        status: 204
    })
    @HttpCode(204)
    deleteOne(id: string): Promise<void> {
        return this.jobService.deleteOne(id);
    }
}
