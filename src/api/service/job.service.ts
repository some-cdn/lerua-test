import {Job} from "../../schedule/job/job";
import {CreateJobDto, UpdateJobDto} from "../dto/job.dto";
import {TypeormStorage} from "../../schedule/storage/typeorm-storage/typeorm.storage";
import {Inject, Injectable} from "@nestjs/common";
import {Connection, Repository} from "typeorm";
import {SCHEDULER_STORAGE} from "../../schedule/constant";

@Injectable()
export class JobService {
    private repository: Repository<Job>

    constructor(
        @Inject(SCHEDULER_STORAGE) private storage: TypeormStorage,
        connection: Connection
    ) {
        this.repository = connection.getRepository(Job);
    }

    getAll(): Promise<Job[]> {
        return this.repository.find({
            transaction: false
        });
    }

    getOne(id: string): Promise<Job> {
        return this.repository.findOneOrFail({
            transaction: false,
            where: {id}
        });
    }

    async updateOne(id: string, dto: UpdateJobDto): Promise<Job> {
        return await this.storage.add(this.repository.create(dto))
    }

    async createOne(dto: CreateJobDto): Promise<Job> {
        return this.storage.add(this.repository.create(dto));
    }

    async deleteOne(id: string): Promise<void> {
        const job = await this.repository.findOneOrFail({where: {id}})
        await this.storage.clear(job);
    }
}