import {HttpMethod} from "../../schedule/job/job";
import {ApiProperty, OmitType} from "@nestjs/swagger";
import {IsBoolean, IsEnum, IsJSON, IsObject, IsUUID, Matches, ValidateIf} from "class-validator";

export class CreateJobDto {
    @ApiProperty({
        format: 'uuid',
        required: false
    })
    @IsUUID(4, {})
    id: string;
    @ApiProperty({
        example: '* * * * *',
        pattern: '(@(annually|yearly|monthly|weekly|daily|hourly|reboot))|(@every (\\d+(ns|us|µs|ms|s|m|h))+)|((((\\d+,)+\\d+|(\\d+(\\/|-)\\d+)|\\d+|\\*) ?){5,7})'
    })
    @Matches(/(@(annually|yearly|monthly|weekly|daily|hourly|reboot))|(@every (\d+(ns|us|µs|ms|s|m|h))+)|((((\d+,)+\d+|(\d+(\/|-)\d+)|\d+|\*) ?){5,7})/)
    cron: string;
    @ApiProperty({
        example: 'http://test.com',
        pattern: 'https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)\n'
    })
    @Matches(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/)
    url: string;
    @ApiProperty({
        enum: HttpMethod
    })
    @IsEnum(HttpMethod)
    method: HttpMethod;
    @ApiProperty({
        type: 'string',
        nullable: true
    })
    @IsJSON()
    @ValidateIf((object, value) => value !== null)
    body: string | null;
    @ApiProperty({
        type: Object
    })
    @IsObject()
    headers: Record<string, string>;
    @ApiProperty()
    @IsBoolean()
    isReusable: boolean
}


export class UpdateJobDto extends OmitType(CreateJobDto, ['id']) {

}