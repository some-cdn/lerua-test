import {DynamicModule, Inject, Module, OnApplicationBootstrap, OnApplicationShutdown} from "@nestjs/common";
import {Constructor} from "@nestjs/common/utils/merge-with-values.util";
import {Storage} from "./storage/storage";
import {
    SCHEDULER_EXECUTOR,
    SCHEDULER_PARAMS,
    SCHEDULER_RUNNER,
    SCHEDULER_STORAGE,
    SCHEDULER_TIME_CALCULATOR
} from "./constant";
import {Executor} from "./executor/executor";
import {JobRunner} from "./job-runner/job-runner";
import {TimeCalculator} from "./time-calculator/time-calculator";

export interface ScheduleModuleOptions {
    storage: Constructor<Storage>;
    executor: Constructor<Executor>;
    runner: Constructor<JobRunner>;
    timeCalculator: Constructor<TimeCalculator>;
    params: ScheduleModuleParams;
}

export interface ScheduleModuleParams {
    timeout: number;
}

@Module({})
export class SchedulerModule implements OnApplicationBootstrap, OnApplicationShutdown {
    constructor(
        @Inject(SCHEDULER_EXECUTOR) private executor: Executor
    ) {
    }

    onApplicationBootstrap() {
        void this.executor.start();
    }

    async onApplicationShutdown() {
        await this.executor.stop();
    }

    static create(options: ScheduleModuleOptions): DynamicModule {
        return {
            global: true,
            module: SchedulerModule,
            exports: [
                SCHEDULER_STORAGE,
                SCHEDULER_EXECUTOR,
            ],
            imports: [],
            controllers: [],
            providers: [
                {provide: SCHEDULER_STORAGE, useClass: options.storage},
                {provide: SCHEDULER_EXECUTOR, useClass: options.executor},
                {provide: SCHEDULER_PARAMS, useValue: options.params},
                {provide: SCHEDULER_RUNNER, useClass: options.runner},
                {provide: SCHEDULER_TIME_CALCULATOR, useClass: options.timeCalculator}
            ],
        }
    }
}