import {JobRunner} from "./job-runner";
import {Job} from "../job/job";
import fetch from "node-fetch";
import {Injectable, Logger} from "@nestjs/common";

@Injectable()
export class FetchJobRunner implements JobRunner {
    private logger: Logger = new Logger(this.constructor.name);

    async run(job: Job): Promise<boolean> {
        try {
            this.logger.debug(`Starting execute job '${job.url}'`);
            await fetch(job.url, {
                method: job.method,
                headers: job.headers,
                body: job.body ? JSON.parse(job.body) : undefined
            });
            this.logger.debug(`Job '${job.url}' successfully executed`);
        } catch (e) {
            this.logger.error(`Failed to run job (${job.id})`, e);
            return false;
        }
        return true;
    }

}