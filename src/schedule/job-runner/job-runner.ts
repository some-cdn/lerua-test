import {Job} from "../job/job";

export interface JobRunner {
    run(job: Job): Promise<boolean>;
}