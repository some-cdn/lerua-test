import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {generateString} from "@nestjs/typeorm";
import {ApiProperty} from "@nestjs/swagger";

export enum HttpMethod {
    GET = 'GET',
    POST = 'POST',
    DELETE = 'DELETE',
    PATCH = 'PATCH',
}

@Entity()
export class Job {

    @PrimaryGeneratedColumn('uuid')
    @ApiProperty({
        example: '123e4567-e89b-12d3-a456-426614174000'
    })
    id: string;

    @Column('varchar')
    @ApiProperty({
        example: '* * * * *'
    })
    cron: string;
    @Column('varchar')
    @ApiProperty({
        example: 'http://test.com'
    })
    url: string;
    @Column('varchar')
    @ApiProperty({})
    method: HttpMethod;
    @Column('varchar', {
        nullable: true
    })
    @ApiProperty({
        example: '{"data": "value"}',
        nullable: true
    })
    body: string | null = null;
    @Column('simple-json')
    @ApiProperty({
        example: {
            "Content-Type": "application/json"
        }
    })
    headers: Record<string, string>;
    @Column('boolean')
    @ApiProperty({})
    isReusable: boolean

    constructor(
        cron: string,
        url: string,
        method: HttpMethod,
        body: string,
        headers: Record<string, string>,
        isReusable: boolean
    ) {
        this.cron = cron;
        this.url = url;
        this.method = method;
        this.body = body;
        this.headers = headers;
        this.isReusable = isReusable;
        this.id = generateString();
    }

}