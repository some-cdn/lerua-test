import {Executor} from "./executor";
import {Storage} from "../storage/storage";
import {Job} from "../job/job";
import {Inject, Injectable, Logger} from "@nestjs/common";
import {SCHEDULER_PARAMS, SCHEDULER_RUNNER, SCHEDULER_STORAGE} from "../constant";
import {ScheduleModuleParams} from "../module";
import {JobRunner} from "../job-runner/job-runner";

@Injectable()
export class BaseExecutor implements Executor {
    private logger: Logger = new Logger(this.constructor.name);

    constructor(
        @Inject(SCHEDULER_STORAGE) private storage: Storage,
        @Inject(SCHEDULER_RUNNER) private runner: JobRunner,
        @Inject(SCHEDULER_PARAMS) private params: ScheduleModuleParams) {
    }

    private isStopped = false;

    async start() {
        this.logger.log(`Schedule executor started`)
        while (!this.isStopped) {
            const job = await this.storage.getJobToExecute();


            if (job) {
                this.logger.log(`Find job to execute: ${job.id}`)
                const result = await this.execute(job)

                if (!result) {
                    await this.storage.reinsert(job, true);
                } else if (job.isReusable) {
                    await this.storage.reinsert(job);
                } else {
                    await this.storage.clear(job);
                }
            }

            await new Promise<void>(resolve => setTimeout(() => resolve(), this.params.timeout))
        }
    }

    async execute(job: Job): Promise<boolean> {
        return this.runner.run(job);
    }

    stop() {
        this.logger.log(`Schedule executor stopped`)
        this.isStopped = true;
    }
}

