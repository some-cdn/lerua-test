export interface Executor {
    start(): Promise<void>;

    stop(): void;
}
