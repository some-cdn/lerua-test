import {Job} from "../job/job";

export interface Storage {
    getJobToExecute(): Promise<Job | null>;

    clear(job: Job): Promise<void>;

    add(job: Job): Promise<Job>;

    update(job: Job): Promise<Job>;

    reinsert(job: Job, now?: boolean): Promise<void>;
}
