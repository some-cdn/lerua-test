import {Column, Entity, Index, PrimaryColumn} from "typeorm";

@Entity()
export class Schedule {
    @Column('datetime')
    @Index()
    executeAt: Date;
    @PrimaryColumn('uuid')
    jobId: string;

    @Column('boolean', {default: false})
    isRunning: boolean;
}