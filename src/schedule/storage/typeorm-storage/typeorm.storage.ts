import {Storage} from "../storage";
import {Connection, LessThanOrEqual, Repository} from "typeorm";
import {Job} from "../../job/job";
import {Schedule} from "./schedule";
import {Inject, Injectable} from "@nestjs/common";
import {TimeCalculator} from "../../time-calculator/time-calculator";
import {SCHEDULER_TIME_CALCULATOR} from "../../constant";

@Injectable()
export class TypeormStorage implements Storage {
    private jobRepository: Repository<Job>;
    private scheduleRepository: Repository<Schedule>;

    private transactionLock: Promise<void> | null = null;

    constructor(
        private connection: Connection,
        @Inject(SCHEDULER_TIME_CALCULATOR) private timeCalculator: TimeCalculator
    ) {
        this.jobRepository = this.connection.getRepository(Job);
        this.scheduleRepository = this.connection.getRepository(Schedule);
    }

    async clear(job: Job): Promise<void> {
        await this.runInTransaction(async () => {
            await this.scheduleRepository.delete({jobId: job.id})
            await this.jobRepository.delete({id: job.id})
        })
    }

    reinsert(job: Job, now?: boolean): Promise<void> {
        return this.runInTransaction(async () => {
            const executeAt = now ? new Date() : this.timeCalculator.calculateNextRunTime(job);
            await this.scheduleRepository.update(
                {jobId: job.id},
                {executeAt, isRunning: false}
            );
        });
    }

    async getJobToExecute(): Promise<Job | null> {
        const entry = await this.runInTransaction(async () => {
            const entry = (await this.scheduleRepository.find({
                transaction: false,
                where: {
                    isRunning: false,
                    executeAt: LessThanOrEqual(new Date())
                },
                order: {
                    executeAt: {direction: 'desc'}
                },
                take: 1,
            }))[0];

            if (!entry) {
                return;
            }

            await this.scheduleRepository.update({jobId: entry.jobId}, {isRunning: true});
            return entry;
        });

        if (!entry) {
            return null;
        }

        return await this.jobRepository.findOne({where: {id: entry.jobId}, transaction: false});
    }

    async add(job: Job) {
        return await this.runInTransaction(async () => {
            const executeAt = this.timeCalculator.calculateNextRunTime(job);
            job = await this.jobRepository.save(job, {
                transaction: false
            });
            await this.scheduleRepository.save({executeAt, jobId: job.id}, {transaction: false})
            return job;
        })
    }

    async update(job: Job) {
        return await this.runInTransaction(async () => {
            job = await this.jobRepository.save(job, {transaction: false});
            await this.scheduleRepository.update({jobId: job.id}, {executeAt: this.timeCalculator.calculateNextRunTime(job)})
            return job;
        })
    }

    private async runInTransaction<T>(callback: () => Promise<T>): Promise<T> {
        let resolveLock: Function = () => null;
        let isTransactionStarted = false;
        try {
            if (this.transactionLock) {
                await this.transactionLock;
            }
            this.transactionLock = new Promise(resolve => {
                resolveLock = resolve;
            });
            this.transactionLock.then(() => this.transactionLock = null);


            await this.executeTransactionBegin();
            await this.executeLock();
            const result = await callback();
            await this.executeTransactionCommit();
            resolveLock()
            return result;
        } catch (e) {
            if (resolveLock) {
                resolveLock()
            }
            if (isTransactionStarted) {
                await this.executeTransactionRollback();
            }
            throw e;
        }
    }

    private async executeTransactionBegin() {
        if (this.connection.options.type === 'sqlite') {
            await this.connection.query('BEGIN EXCLUSIVE TRANSACTION')
        } else {
            // todo add transaction for other
        }
    }

    private async executeTransactionCommit() {
        await this.connection.query('COMMIT')
    }

    private async executeTransactionRollback() {
        await this.connection.query('ROLLBACK')
    }

    private async executeLock() {
        if (this.connection.options.type === 'sqlite') {
            return;
        } else {
            await this.connection.query(`LOCK TABLE ${this.scheduleRepository.metadata.tablePath} IN EXCLUSIVE MODE`);
        }
    }
}
