import {TimeCalculator} from "./time-calculator";
import {Job} from "../job/job";
import cronParser from "cron-parser";
import {Injectable} from "@nestjs/common";

@Injectable()
export class CronTimeCalculator implements TimeCalculator {
    calculateNextRunTime(job: Job): Date {
        return new Date(cronParser.parseExpression(job.cron).next().toISOString());
    }
}
