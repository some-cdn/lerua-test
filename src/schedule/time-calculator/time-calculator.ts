import {Job} from "../job/job";

export interface TimeCalculator {
    calculateNextRunTime(job: Job): Date;
}