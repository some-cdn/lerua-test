import {SchedulerModule} from "./module";
import {Test} from "@nestjs/testing";
import {BaseExecutor} from "./executor/base-executor";
import {TypeormStorage} from "./storage/typeorm-storage/typeorm.storage";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Executor} from "./executor/executor";
import {Storage} from "./storage/storage";
import {INestApplication, Injectable} from "@nestjs/common";
import {SCHEDULER_EXECUTOR, SCHEDULER_RUNNER, SCHEDULER_STORAGE, SCHEDULER_TIME_CALCULATOR} from "./constant";
import {HttpMethod, Job} from "./job/job";
import {EntityManager} from "typeorm";
import {Schedule} from "./storage/typeorm-storage/schedule";
import {JobRunner} from "./job-runner/job-runner";
import {parseExpression} from "cron-parser";
import {TimeCalculator} from "./time-calculator/time-calculator";
import {CronTimeCalculator} from "./time-calculator/cron.time-calculator";
import Mock = jest.Mock;


const TIMEOUT = 10;


@Injectable()
class TestJobRunner implements JobRunner {
    run = jest.fn(async job => {
        console.log('Running job', job);
        return true;
    })
}

describe(SchedulerModule, () => {
    let app: INestApplication;
    let executor: Executor;
    let storage: Storage;
    let entityManager: EntityManager;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                SchedulerModule.create({
                    params: {
                        timeout: TIMEOUT
                    },
                    executor: BaseExecutor,
                    storage: TypeormStorage,
                    runner: TestJobRunner,
                    timeCalculator: CronTimeCalculator
                }),
                TypeOrmModule.forRoot({
                    type: "sqlite",
                    database: ":memory:",
                    synchronize: true,
                    entities: [Job, Schedule],
                    autoLoadEntities: true,
                }) // in memory storage,
            ],
        }).compile();

        app = moduleRef.createNestApplication();
        storage = app.get(SCHEDULER_STORAGE);
        executor = app.get(SCHEDULER_EXECUTOR);
        entityManager = app.get(EntityManager);
        await app.init();
    });

    describe(TypeormStorage.name, () => {
        it('should add job to orm', async () => {
            const job = new Job(
                '',
                'http://test.com',
                HttpMethod.GET,
                'asd',
                {},
                true
            );
            await storage.add(job);
            const res = await entityManager.find(Job);
            expect(res[0]).toEqual(job)
        })
        it('should remove job from orm', async () => {
            const job = new Job(
                '',
                'http://test.com',
                HttpMethod.GET,
                'asd',
                {},
                true
            );
            await storage.add(job);
            const res = await entityManager.find(Job);
            expect(res[0]).toEqual(job)
            await storage.clear(job);
            const emptyJobsList = await entityManager.find(Job);
            expect(emptyJobsList).toHaveLength(0)
        })
        it('should update job at orm', async () => {
            const job = new Job(
                '',
                'http://test.com',
                HttpMethod.GET,
                'asd',
                {},
                true
            );
            await storage.add(job);
            const res = await entityManager.find(Job);
            expect(res[0]).toEqual(job);

            job.url = 'http://test2.com';

            await storage.update(job);
            const updatedJobList = await entityManager.find(Job);
            expect(updatedJobList[0].url).toEqual('http://test2.com')
        })
        it('should correctly create schedule record', async () => {
            const job = new Job(
                '5 * * * *',
                'http://test.com',
                HttpMethod.GET,
                'asd',
                {},
                true
            );

            const cron = parseExpression('5 * * * *');
            const nextRunDate = new Date(cron.next().toISOString());
            await storage.add(job);
            const res = await entityManager.find(Schedule);
            expect(res[0]).toEqual({
                jobId: job.id,
                executeAt: nextRunDate,
                isRunning: false
            } as Schedule)
        })
        it('should remove schedule record on deleting job', async () => {
            const job = new Job(
                '5 * * * *',
                'http://test.com',
                HttpMethod.GET,
                'asd',
                {},
                true
            );

            const cron = parseExpression('5 * * * *');
            const nextRunDate = new Date(cron.next().toISOString());
            await storage.add(job);
            const res = await entityManager.find(Schedule);
            expect(res[0]).toEqual({
                jobId: job.id,
                executeAt: nextRunDate,
                isRunning: false
            } as Schedule)

            await storage.clear(job);

            const emptyScheduleList = await entityManager.find(Schedule);

            expect(emptyScheduleList).toHaveLength(0)
        })
    });

    describe(BaseExecutor.name, () => {
        it('should execute job once', async () => {
            const runner: TestJobRunner = app.get(SCHEDULER_RUNNER);
            const calculator: TimeCalculator = app.get(SCHEDULER_TIME_CALCULATOR)
            const job = new Job(
                '* * * * *',
                'http://test123.com',
                HttpMethod.GET,
                'asd',
                {},
                true
            );

            calculator.calculateNextRunTime = jest.fn(() => {
                const mock = (calculator.calculateNextRunTime as Mock).mock;
                if (mock.calls.length <= 1) {
                    return new Date()
                } else {
                    const date = new Date();
                    date.setFullYear(new Date().getFullYear() + 1)
                    return date
                }
            });

            await storage.add(job);

            await new Promise<void>(resolve => setTimeout(() => resolve(), 500));

            expect(runner.run.mock.calls.length).toBe(1);
        });

        it('should execute job twice', async () => {
            const runner: TestJobRunner = app.get(SCHEDULER_RUNNER);
            const calculator: TimeCalculator = app.get(SCHEDULER_TIME_CALCULATOR)
            const job = new Job(
                '* * * * *',
                'http://test123.com',
                HttpMethod.GET,
                'asd',
                {},
                true
            );

            calculator.calculateNextRunTime = jest.fn(() => {
                const mock = (calculator.calculateNextRunTime as Mock).mock;
                if (mock.calls.length <= 2) {
                    return new Date()
                } else {
                    const date = new Date();
                    date.setFullYear(new Date().getFullYear() + 1)
                    return date
                }
            });

            await storage.add(job);

            await new Promise<void>(resolve => setTimeout(() => resolve(), 500));

            expect(runner.run.mock.calls.length).toBe(2);
        });
    });
});
