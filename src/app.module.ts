import {Module} from "@nestjs/common";
import {ApiModule} from "./api/module";
import {SchedulerModule} from "./schedule/module";
import {BaseExecutor} from "./schedule/executor/base-executor";
import {TypeormStorage} from "./schedule/storage/typeorm-storage/typeorm.storage";
import {FetchJobRunner} from "./schedule/job-runner/fetch.job-runner";
import {CronTimeCalculator} from "./schedule/time-calculator/cron.time-calculator";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Job} from "./schedule/job/job";
import {Schedule} from "./schedule/storage/typeorm-storage/schedule";

@Module({
    imports: [
        ApiModule,
        TypeOrmModule.forRoot({
            type: "sqlite",
            database: ":memory:",
            synchronize: true,
            entities: [Job, Schedule],
            autoLoadEntities: true,
        }),
        SchedulerModule.create({
            params: {
                timeout: 20
            },
            executor: BaseExecutor,
            storage: TypeormStorage,
            runner: FetchJobRunner,
            timeCalculator: CronTimeCalculator
        })
    ]
})
export class AppModule {

}